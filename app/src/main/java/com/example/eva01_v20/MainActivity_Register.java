package com.example.eva01_v20;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity_Register extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__register);
    }

    //Método para botón Registrar
    public void Registrar(View view){
        Intent registrar = new Intent(this, MainActivity.class);
        startActivity(registrar);
    }

}