package com.example.eva01_v20;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity_Search extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__search);
    }

    //Método para ir a ver todas las evaluaciones
    public void Ver (View view){
        Intent ver = new Intent(this, MainActivity_View.class);
        startActivity(ver);
    }

}