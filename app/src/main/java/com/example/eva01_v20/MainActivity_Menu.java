package com.example.eva01_v20;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity_Menu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__menu);
    }

    //Método para icono Evaluaciones
    public void Evaluaciones (View view){
        Intent evaluaciones = new Intent(this, MainActivity_MenuEV.class);
        startActivity(evaluaciones);
    }

}