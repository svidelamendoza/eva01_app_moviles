package com.example.eva01_v20;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Método en action Bar
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
    }



    //Método Botón Registrarse
    public void Registrarse(View view){
            Intent registrarse = new Intent(this, MainActivity_Register.class);
            startActivity(registrarse);

    }

    //Método Botón login
    public void Log(View view) {
        Intent entrar = new Intent(this, MainActivity_Menu.class);
        startActivity(entrar);
    }
}

